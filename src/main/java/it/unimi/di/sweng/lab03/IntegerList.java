package it.unimi.di.sweng.lab03;

public class IntegerList {
	
	private IntegerNode head = null;
	private IntegerNode tail = null;
	
	public IntegerList(){}
	
	public IntegerList(String s) {
		StringBuilder sb = new StringBuilder(s);
		for(int i=0; i<sb.length(); i = i+2){
			int v =(int) sb.charAt(i)-48;
			if(v > 0 && v < 9)
					addLast(v);
			else
				throw new IllegalArgumentException();
			
			//addLast((int) sb.charAt(i)-48);
		}
		
	}

	public String toString(){
		String result = "[";
		IntegerNode currentNode = head;
		int i =0;
		while(currentNode != null){
			if(i++ > 0)
				result += " ";
			result += currentNode.getValue();
			currentNode = currentNode.next();
		}
		return result + "]";
	}

	public void addLast(int value) {
		if(head == null)
			head = tail = new IntegerNode(value);
		else{
			IntegerNode node = new IntegerNode(value);;
			tail.setNext(node);
			tail = node;
		}
	}

	public void addFirst(int s) {
	
		if(head == null)
			head = tail = new IntegerNode(s);
		else{
			IntegerNode node = new IntegerNode(s);;
			node.setNext(head);
			head = node;
		}
		
	}
	
}
